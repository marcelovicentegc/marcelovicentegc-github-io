[![Build Status](https://travis-ci.com/marcelovicentegc/marcelovicentegc.github.io.svg?branch=master-source)](https://travis-ci.com/marcelovicentegc/marcelovicentegc.github.io)

# marcelovicentegc.github.io

## Development

- Clone this repo: `git clone https://github.com/marcelovicentegc/marcelovicentegc.github.io`
- Make sure you have a `.env` file that follows the `.env.example` one, correctly set
- Install dependencies: `npm i`
- Start the application: `npm start`
