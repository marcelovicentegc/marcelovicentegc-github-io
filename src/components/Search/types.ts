export interface Index {
  excerpt?: string
  title?: string
  slug?: string
}
