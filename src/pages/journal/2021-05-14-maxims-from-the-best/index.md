---
title: "Maxims from the best"
---

> Never blame yourself for being too kind.
>
> _Yasmin Santana_

---

> Easy choices, hard life. Hard choices, easy life.
>
> _Jerzy Gregorek_

---

> Freedom is to understand yourself, your emotions, who you are, and not to emprision yourself by "wanting to be something".
>
> _Krishna Murti_

---

> Use context, not control. Inspire and lead people - don't micro-manage them - through setting the context.
>
> - What are we trying to do?
> - What are the constraints?
> - Is it a big problem? Is it a little problem?
> - Must it be done right, or we do an approximate version and fix it later?
> - What are the behavioral norms in terms of honesty and sharing and forthrightness?
>
> _Reed Hastings_

---

> Don't criticize, condene or complain about others. If you know something good about someone, say it to others.
>
> _Dale Carnegie_

---

> Make money with your mind, not with your time... Product leverage is the key for wealth.
>
> _Naval Ravikant_

---

> The hard path is the unlearning path, the path where you start from the bottom of the mountain, in which you realize that in order to enjoy your full potential, you need to start from zero.
>
> _Naval Ravikant_
