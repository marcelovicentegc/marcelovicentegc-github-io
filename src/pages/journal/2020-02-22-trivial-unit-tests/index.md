---
title: "Unit testing your web app with Jest and React Testing Library: the trivial"
---

It was a sunny Saturday morning when a colleague introduced me and some other dears colleagues, unit testing with Jest and the React Testing Library. Since that day, I was using Enzyme and Jest as my tools to go testing. I had never heard of the React Testing Library. I gave it the shot, and I'm implementing it in every project I touch. A couple of months have passed since that day, and I'm one of the few that went heads and hands-on on it. As soon as I noticed that, I knew where the problem was.

[If you want to learn more about this story, check it out on my Medium page](https://medium.com/@marcelovicentegc/unit-testing-your-web-app-with-jest-and-react-testing-library-the-trivial-4f432eaec33d)
